export const AuthConfig = () => ({
    jwksUrl: process.env.KEYCLOAK_JWKS_URL,
    clientId: process.env.KEYCLOAK_CLIENT_ID,
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
    adminUser: process.env.KEYCLOAK_ADMIN_USER,
    adminPassword: process.env.KEYCLOAK_ADMIN_PASSWORD,
    realmName: process.env.KEYCLOAK_REALM_NAME,
    baseUrl: process.env.KEYCLOAK_BASE_URL
})

export const DatabaseConfig = () => ({
    database: {
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        name: process.env.DB_NAME
    }
})

export const BrokerConfig = () => ({
    broker: {
        host: process.env.BROKER_HOST
    }
})

export const ServiceConfig = () => ({
    service: {
        branchUrl: process.env.BRANCH_SERVICE_URL
    }
})

export const SystemConfig = () => ({
    customer: {
        sequenceName: process.env.CUSTOMER_SEQUENCE_NAME
    }
})