import { Module, Global, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { BankSharedModule, SagaState } from 'bank-shared-lib';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BrokerConfig, DatabaseConfig, ServiceConfig, SystemConfig, AuthConfig} from './configs/configuration';
import { GenerateCifNoHandler } from './commands/handlers/generate-cif-no.handler';
import { SequenceCifNoGenerator } from './implementations/sequence-cif-no-generator.impl';
import { CustomerRepository } from './repositories/customer.repository';
import { Customer } from './entities/customer.entity';
import { CustomerServiceImpl } from './implementations/services/customer-service.impl';
import { CustomerFactory } from './factories/customer.factory';
import { RuleContext } from './implementations/rule-context.impl';
import { NewCustomerValidation } from './implementations/validations/new-customer-validation.impl';
import { CreateCustomerHandler } from './commands/handlers/create-customer.handler';
import { CustomerQueryServiceImpl } from './implementations/services/customer-query-service.impl';
import { BankCreateCustomerSaga } from './sagas/bank-create-customer.saga';
import { BankCustomerCreateCifHandler } from './commands/handlers/bank-customer-create-cif.handler';
import { BankCustomerCreateHandler } from './commands/handlers/bank-customer-create.handler';
import { BankCustomerController } from './controllers/bank-customer.controller';
import { TellerAuthMiddleware } from './middlewares/teller-auth.middleware';
import { BlacklistCustomerHandler } from './commands/handlers/blacklist-customer.handler';
import { RollbackBlacklistCustomerHandler } from './commands/handlers/rollback-blacklist-customer.handler';
import { UnblacklistCustomerHandler } from './commands/handlers/unblacklist-customer.handler';
import { RollbackUnblacklistCustomerHandler } from './commands/handlers/rollback-unblacklist-customer.handler';
import { BlacklistCustomerSaga } from './sagas/blacklist-customer.saga';
import { UnblacklistCustomerSaga } from './sagas/unblacklist-customer.saga';
import { BlacklistStatusRule, UnblacklistStatusRule, ApproveStatusRule } from './implementations/validations/status-validation.impl';


const sharedSagaFactory = [
  {
    provide: 'BankCreateCustomerSaga',
    useClass: BankCreateCustomerSaga
  },
  {
    provide: 'BlacklistCustomerSaga',
    useClass: BlacklistCustomerSaga
  },
  {
    provide: 'UnblacklistCustomerSaga',
    useClass: UnblacklistCustomerSaga
  }
]
@Global()
@Module({
  imports: [
    BankSharedModule.registerSaga({
      useFactory: (repository, brokerPublisher) => ({
        sagaList: [
          new BankCreateCustomerSaga(repository, brokerPublisher),
          new BlacklistCustomerSaga(repository, brokerPublisher),
          new UnblacklistCustomerSaga(repository, brokerPublisher)
        ]
      }),
      inject: [getRepositoryToken(SagaState), 'BrokerPublisher']
    }),
    BankSharedModule.registerHandler({
      useFactory: (brokerPublisher, cifNoGenerator, customerService) => ({
        commandList: [
          new GenerateCifNoHandler(brokerPublisher, cifNoGenerator),
          new CreateCustomerHandler(brokerPublisher, customerService),
          new BankCustomerCreateCifHandler(brokerPublisher, cifNoGenerator),
          new BankCustomerCreateHandler(brokerPublisher, customerService),
          new BlacklistCustomerHandler(brokerPublisher, customerService),
          new RollbackBlacklistCustomerHandler(brokerPublisher, customerService),
          new UnblacklistCustomerHandler(brokerPublisher, customerService),
          new RollbackUnblacklistCustomerHandler(brokerPublisher, customerService)
        ]
      }),
      inject: ['BrokerPublisher', 'CifNoGenerator', 'CustomerService']
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        return {
          type: 'postgres',
          host: configService.get<string>('database.host'),
          port: configService.get<number>('database.port'),
          username: configService.get<string>('database.username'),
          password: configService.get<string>('database.password'),
          database: configService.get<string>('database.name'),
          entities: [Customer, SagaState],
          synchronize: true
        }
      },
      inject: [ConfigService]
    }),
    TypeOrmModule.forFeature([CustomerRepository]),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [BrokerConfig, DatabaseConfig, ServiceConfig, SystemConfig, AuthConfig]
    })
  ],
  controllers: [AppController, BankCustomerController],
  providers: [
    AppService,
    {
      provide: 'CifNoGenerator',
      useClass: SequenceCifNoGenerator
    },
    {
      provide: 'CustomerFactory',
      useClass: CustomerFactory
    },
    {
      provide: 'NewCustomerValidation',
      useFactory: (customerRepository) => {
        return new RuleContext([new NewCustomerValidation(customerRepository)])
      },
      inject: [CustomerRepository]
    },
    {
      provide: 'CustomerService',
      useFactory: (customerRepository, customerFactory, queryService) => {
        return new CustomerServiceImpl(customerRepository, customerFactory, queryService);
      },
      inject: [CustomerRepository, 'CustomerFactory', 'CustomerQueryService']
    },
    {
      provide: 'BlacklistRule',
      useFactory: () => {
        return new RuleContext([new BlacklistStatusRule()])
      }
    },
    {
      provide: 'UnblacklistRule',
      useFactory: () => {
        return new RuleContext([new UnblacklistStatusRule()])
      }
    },
    {
      provide: 'ApprovalRule',
      useFactory: () => {
        return new RuleContext([new ApproveStatusRule()])
      }
    },
    {
      provide: 'CustomerQueryService',
      useFactory: (customerRepository, blacklistRule, unblacklistRule, approvalRule) => {
        return new CustomerQueryServiceImpl(customerRepository, blacklistRule, unblacklistRule, approvalRule);
      },
      inject: [CustomerRepository, 'BlacklistRule', 'UnblacklistRule', 'ApprovalRule']
    },
    ...sharedSagaFactory
  ],
  exports: [
    {
      provide: 'CifNoGenerator',
      useClass: SequenceCifNoGenerator
    },
    {
      provide: 'CustomerService',
      useFactory: (customerRepository, customerFactory, queryService) => {
        return new CustomerServiceImpl(customerRepository, customerFactory, queryService);
      },
      inject: [CustomerRepository, 'CustomerFactory', 'CustomerQueryService']
    },
    ...sharedSagaFactory
  ]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(TellerAuthMiddleware)
      .forRoutes(BankCustomerController)
  }
}
