import { EntityRepository, Raw } from 'typeorm';
import { Customer } from './../entities/customer.entity';
import { PostgresRepository } from './postgres.repository';

@EntityRepository(Customer)
export class CustomerRepository extends PostgresRepository<Customer> {
    async searchQuery(queryParam: object): Promise<Customer[]> {
        const findOptions = {}
        for (const attribute of Object.keys(queryParam)) {
            if (attribute === 'branchId') {
                findOptions[attribute] = queryParam[attribute];
            } else {
                findOptions[attribute] = Raw(alias => {
                    return `lower(${alias}) like lower('%${queryParam[attribute]}%')`
                });
            }
        }
        return await this.find(findOptions);
    }
}