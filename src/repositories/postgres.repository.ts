import { Repository, EntityRepository } from 'typeorm'

@EntityRepository()
export class PostgresRepository<T> extends Repository<T> {
    async getSequence(sequenceName: string): Promise<string> {
        const queryString = "select nextval('" + sequenceName + "')";
        const latestSeq = await this.query(queryString);
        return '' + latestSeq[0]['nextval'];
    }
}