import { Controller, Post, Body, Inject, Param } from "@nestjs/common";
import { CreateCustomerDto } from "./../dto/create-customer.dto";
import { AuthContext, UserContext } from "bank-shared-lib";
import { BankCreateCustomerSaga } from "./../sagas/bank-create-customer.saga";
import { CustomerQueryService } from "./../interfaces/services/customer-query-service.interface";
import { BlacklistCustomerSaga } from "./../sagas/blacklist-customer.saga";
import { UnblacklistCustomerSaga } from "./../sagas/unblacklist-customer.saga";
import { CustomerService } from "./../interfaces/services/customer-service.interface";

@Controller()
export class BankCustomerController {
    constructor(
        @Inject('CustomerQueryService') private readonly queryService: CustomerQueryService,
        @Inject('BankCreateCustomerSaga') private readonly bankCreateCustomerSaga: BankCreateCustomerSaga,
        @Inject('BlacklistCustomerSaga') private readonly blacklistSaga: BlacklistCustomerSaga,
        @Inject('UnblacklistCustomerSaga') private readonly unblacklistSaga: UnblacklistCustomerSaga,
        @Inject('CustomerService') private readonly customerService: CustomerService
    ) {}

    @Post('/api/customer/create')
    async createCustomer(
        @Body() createCustomerDto: CreateCustomerDto,
        @AuthContext() userContext: UserContext
    ) {
        createCustomerDto.branchCode = userContext.branchCode;
        createCustomerDto.registeredBy = userContext.email;

        return await this.bankCreateCustomerSaga.startSaga(createCustomerDto, 'bankCustomerCreateStartedEvent');
    }

    @Post('/api/customer/blacklist/:cifNo')
    async blockCustomer(
        @Param('cifNo') cifNo: string
    ) {
        const customer = await this.queryService.findValidCustomerForBlacklist(cifNo);
        return await this.blacklistSaga.startSaga(customer, 'blacklistCustomerStartedEvent');
    }

    @Post('/api/customer/unblacklist/:cifNo')
    async unblockCustomer(
        @Param('cifNo') cifNo: string
    ) {
        const customer = await this.queryService.findValidCustomerForUnblacklist(cifNo);
        return await this.unblacklistSaga.startSaga(customer, 'unblacklistCustomerStartedEvent');
    }

    @Post('/api/customer/approve/:cifNo')
    async approveCustomer(
        @Param('cifNo') cifNo: string
    ) {
        return await this.customerService.approveCustomer(cifNo);
    }
}