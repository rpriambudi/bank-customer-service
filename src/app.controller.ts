import { Controller, Get, Param, Inject } from '@nestjs/common';
import { SagaService } from 'bank-shared-lib';
import { CustomerQueryService } from './interfaces/services/customer-query-service.interface';

@Controller()
export class AppController {
  constructor(
    @Inject('CustomerQueryService') private readonly customerQueryService: CustomerQueryService,
    @Inject('SagaService') private readonly sagaService: SagaService
  ) {}

  @Get('/api/customer/:idType/:idNumber/idtype')
  async findCustomerByIdType(@Param('idType') idType: string, @Param('idNumber') idNumber: string ) {
    return await this.customerQueryService.findCustomerByIdTypeAndNumber(idType, idNumber);
  }

  @Get('/api/customer/:cifNo')
  async findCustomerByCifNo(@Param('cifNo') cifNo: string) {
    return await this.customerQueryService.findCustomerByCifNo(cifNo);
  }

  @Get('/api/saga/state/:stateId')
  async findSagaState(
    @Param('stateId') stateId: number
  ) {
    return await this.sagaService.findSagaByStateId(stateId);
  }
}
