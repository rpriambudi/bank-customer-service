import axios from 'axios';
import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer, StatusEnum } from './../entities/customer.entity';
import { CreateCustomerDto } from './../dto/create-customer.dto';
import { CustomerRepository } from './../repositories/customer.repository';
import { CifNoGenerator } from './../interfaces/cif-no-generator.interface';
import { BusinessRule } from './../interfaces/business-rule.interface';

@Injectable()
export class CustomerFactory {
    constructor(
        @InjectRepository(Customer) private readonly customerRepository: CustomerRepository,
        @Inject('CifNoGenerator') private readonly cifNoGenerator: CifNoGenerator,
        @Inject('NewCustomerValidation') private readonly newCustomerValidation: BusinessRule,
        private readonly configService: ConfigService
    ) {}

    async buildNewCustomerData(createCustomerDto: CreateCustomerDto, cifNo?: string): Promise<Customer> {
        await this.newCustomerValidation.validate(createCustomerDto);
        const branchData = await this.getBranchData(createCustomerDto.branchCode);
        const customerData = this.customerRepository.create(createCustomerDto);

        customerData.branchId = branchData.id;
        customerData.status = StatusEnum.Live;

        if (cifNo)
            customerData.cifNo = cifNo;
        else
            customerData.cifNo = await this.cifNoGenerator.generateNewNumber(branchData.branchCode, customerData.idNumber);
            
        return customerData;
    }

    private async getBranchData(branchCode: string): Promise<any> {
        const branchUrl = this.configService.get<string>('service.branchUrl');
        const { data } = await axios.get(`${branchUrl}/api/branch/${branchCode}/code`);
        return data;
    }
}