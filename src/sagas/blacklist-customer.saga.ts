import { BaseSaga, BaseEvent } from 'bank-shared-lib';
import { BlacklistCustomerStartedEvent } from './../events/blacklists/blacklist-customer-started.event';
import { BlacklistCustomerCommand } from './../commands/blacklists/blacklist-customer.command';
import { CustomerBlacklistedEvent } from './../events/blacklists/customer-blacklisted.event';
import { BlacklistCustomerAccountCommand } from './../commands/blacklists/blacklist-customer-account.command';
import { CustomerAccountBlacklistedEvent } from './../events/blacklists/customer-account-blacklisted.event';
import { BlacklistCustomerFailedEvent } from './../events/blacklists/blacklist-customer-failed.event';
import { BlacklistCustomerAccountFailedEvent } from './../events/blacklists/blacklist-customer-account-failed.event';
import { RollbackBlacklistCustomerCommand } from './../commands/blacklists/rollback-blacklist-customer.command';

export class BlacklistCustomerSaga extends BaseSaga {
    getEvents(): BaseEvent[] {
        return [
            new BlacklistCustomerStartedEvent(new BlacklistCustomerCommand()),
            new CustomerBlacklistedEvent(new BlacklistCustomerAccountCommand()),
            new CustomerAccountBlacklistedEvent(null),
            new BlacklistCustomerFailedEvent(null),
            new BlacklistCustomerAccountFailedEvent(new RollbackBlacklistCustomerCommand())
        ]
    }
    
}