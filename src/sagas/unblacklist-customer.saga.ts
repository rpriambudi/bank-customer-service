import { BaseSaga, BaseEvent } from 'bank-shared-lib';
import { UnblacklistCustomerStartedEvent } from 'src/events/unblacklists/unblacklist-customer-started.event';
import { UnblacklistCustomerCommand } from 'src/commands/unblacklists/unblacklist-customer.command';
import { CustomerUnblacklistedEvent } from 'src/events/unblacklists/customer-unblacklisted.event';
import { UnblacklistCustomerAccountCommand } from 'src/commands/unblacklists/unblacklist-customer-account.command';
import { CustomerAccountUnblacklistedEvent } from 'src/events/unblacklists/customer-account-unblacklisted.event';
import { UnblacklistCustomerFailedEvent } from 'src/events/unblacklists/unblacklist-customer-failed.event';
import { UnblacklistCustomerAccountFailedEvent } from 'src/events/unblacklists/unblacklist-customer-account-failed.event';
import { RollbackUnblacklistCustomerCommand } from 'src/commands/unblacklists/rollback-unblacklist-customer.command';

export class UnblacklistCustomerSaga extends BaseSaga {
    getEvents(): BaseEvent[] {
        return [
            new UnblacklistCustomerStartedEvent(new UnblacklistCustomerCommand()),
            new CustomerUnblacklistedEvent(new UnblacklistCustomerAccountCommand()),
            new CustomerAccountUnblacklistedEvent(null),
            new UnblacklistCustomerFailedEvent(null),
            new UnblacklistCustomerAccountFailedEvent(new RollbackUnblacklistCustomerCommand())
        ];
    }

}