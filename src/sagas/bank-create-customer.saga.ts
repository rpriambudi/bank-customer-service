import { BaseSaga, BaseEvent } from 'bank-shared-lib';
import { BankCustomerCreateStartedEvent } from './../events/creates/bank-customer-create-started.event';
import { BankCustomerCreateCifCommand } from './../commands/creates/bank-customer-create-cif.command';
import { BankCustomerCifCreatedEvent } from './../events/creates/bank-customer-cif-created.event';
import { BankCustomerCreateCommand } from './../commands/creates/bank-customer-create.command';
import { BankCustomerCreatedEvent } from './../events/creates/bank-customer-created.event';
import { BankCustomerCreateAccountNoCommand } from './../commands/creates/bank-customer-create-account-no.command';
import { BankCustomerAccountNoCreatedEvent } from './../events/creates/bank-customer-account-no-created.event';
import { BankCustomerCreateAccountCommand } from './../commands/creates/bank-customer-create-account.command';
import { BankCustomerAccountCreatedEvent } from './../events/creates/bank-customer-account-created.event';
import { BankCustomerCreateCifFailedEvent } from 'src/events/creates/bank-customer-create-cif-failed.event';
import { BankCustomerCreateFailedEvent } from 'src/events/creates/bank-customer-create-failed.event';
import { BankCustomerCreateAccountNoFailedEvent } from 'src/events/creates/bank-customer-create-account-no-failed.event';
import { BankCustomerCreateAccountFailedEvent } from 'src/events/creates/bank-customer-create-account-failed.event';

export class BankCreateCustomerSaga extends BaseSaga {
    getEvents(): BaseEvent[] {
        return [
            new BankCustomerCreateStartedEvent(new BankCustomerCreateCifCommand()),
            new BankCustomerCifCreatedEvent(new BankCustomerCreateCommand()),
            new BankCustomerCreatedEvent(new BankCustomerCreateAccountNoCommand()),
            new BankCustomerAccountNoCreatedEvent(new BankCustomerCreateAccountCommand()),
            new BankCustomerAccountCreatedEvent(null),
            new BankCustomerCreateCifFailedEvent(null),
            new BankCustomerCreateFailedEvent(null),
            new BankCustomerCreateAccountNoFailedEvent(null),
            new BankCustomerCreateAccountFailedEvent(null)
        ]
    }
    
}