import { GenericException } from 'bank-shared-lib';

export class CustomerNotFoundException extends GenericException {
    getDisplayCode(): string {
        return 'CUSTOMER_NOT_FOUND';
    }

    getErrorCode(): string {
        return '400202';
    }

    constructor(message: string) {
        super(message);
    }
}