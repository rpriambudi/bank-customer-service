import { GenericException } from 'bank-shared-lib';

export class InvalidStatusException extends GenericException {
    getDisplayCode(): string {
        return 'INVALID_STATUS';
    }

    getErrorCode(): string {
        return '400203';
    }

    constructor(message: string) {
        super(message);
    }
}