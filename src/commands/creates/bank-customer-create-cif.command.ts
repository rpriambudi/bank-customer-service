import { BaseCommand } from 'bank-shared-lib';

export class BankCustomerCreateCifCommand extends BaseCommand {
    getCommandName(): string {
        return 'bankCustomerCreateCifCommand';
    }
    
}