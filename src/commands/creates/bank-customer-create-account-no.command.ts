import { BaseCommand } from 'bank-shared-lib';

export class BankCustomerCreateAccountNoCommand extends BaseCommand {
    getCommandName(): string {
        return 'bankCustomerCreateAccountNoCommand';
    }
    
}