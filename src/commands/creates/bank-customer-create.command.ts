import { BaseCommand } from 'bank-shared-lib';

export class BankCustomerCreateCommand extends BaseCommand {
    getCommandName(): string {
        return 'bankCustomerCreateCommand';
    }
    
}