import { BaseCommand } from 'bank-shared-lib';

export class BankCustomerCreateAccountCommand extends BaseCommand {
    getCommandName(): string {
        return 'bankCustomerCreateAccountCommand';
    }
    
}