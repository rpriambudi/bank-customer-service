import { BaseCommand } from 'bank-shared-lib';

export class UnblacklistCustomerAccountCommand extends BaseCommand {
    getCommandName(): string {
        return 'unblacklistCustomerAccountCommand';
    }
    
}