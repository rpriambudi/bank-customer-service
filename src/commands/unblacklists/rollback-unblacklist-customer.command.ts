import { BaseCommand } from 'bank-shared-lib';

export class RollbackUnblacklistCustomerCommand extends BaseCommand {
    getCommandName(): string {
        return 'rollbackUnblacklistCustomerCommand';
    }
    
}