import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { CustomerService } from './../../interfaces/services/customer-service.interface';

export class BankCustomerCreateHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly customerService: CustomerService
    ) {
        super();
    }

    getCommandName(): string {
        return 'bankCustomerCreateCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { stateId, cifNo } = data;

        try {
            const customer = await this.customerService.createCustomer(data, cifNo);
            await this.brokerPublisher.dispatch('bankCustomerCreatedEvent', { stateId: stateId, customerId: customer.id });
        } catch(error) {
            await this.brokerPublisher.dispatch('bankCustomerCreateFailedEvent', { stateId: stateId, error: error.message });
        }
    }

}