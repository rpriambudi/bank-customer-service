import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { CreateCustomerDto} from './../../dto/create-customer.dto';
import { CifNoGenerator } from './../../interfaces/cif-no-generator.interface';

export class GenerateCifNoHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly cifNoGenerator: CifNoGenerator
    ) {
        super();
    }
    
    getCommandName(): string {
        return 'generateCifNoCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const stateId: number = data.stateId;
        const createCustomerDto: CreateCustomerDto = CreateCustomerDto.buildFromJsonObject(data.customer);

        try {
            const cifNo = await this.cifNoGenerator.generateNewNumber(createCustomerDto.branchCode, createCustomerDto.idNumber);
            const eventData = { stateId: stateId, cifNo: cifNo};
            await this.brokerPublisher.dispatch('cifNoGeneratedEvent', eventData);
        } catch(error) {
            await this.brokerPublisher.dispatch('generateCifNoFailedEvent', { stateId: stateId, error: error.message });
        }
    }
}