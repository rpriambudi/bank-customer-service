import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { CreateCustomerDto} from './../../dto/create-customer.dto';
import { CustomerService } from './../../interfaces/services/customer-service.interface';

export class CreateCustomerHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly customerService: CustomerService
    ) {
        super();
    }
    
    getCommandName(): string {
        return 'createCustomerCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const commandData = data.customer;
        const stateId: number = data.stateId;
        const cifNo: string = data.cifNo;
        const createCustomerDto: CreateCustomerDto = CreateCustomerDto.buildFromJsonObject(commandData);

        try {
            const customer = await this.customerService.createCustomer(createCustomerDto, cifNo);
            const eventData = { stateId: stateId, customerId: customer.id};
            await this.brokerPublisher.dispatch('customerCreatedEvent', eventData);
        } catch(error) {
            let errorData = new Error(error.message);
            if (error.response)
                errorData = new Error(error.response.data.message);

            await this.brokerPublisher.dispatch('createCustomerFailedEvent', { stateId: stateId, error: errorData });
        }
    }
}