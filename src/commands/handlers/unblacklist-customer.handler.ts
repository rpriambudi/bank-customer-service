import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { CustomerService } from './../../interfaces/services/customer-service.interface';

export class UnblacklistCustomerHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly customerService: CustomerService
    ) {
        super();
    }

    getCommandName(): string {
        return 'unblacklistCustomerCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { stateId, cifNo } = data;

        try {
            const customer = await this.customerService.unblacklistCustomer(cifNo);
            await this.brokerPublisher.dispatch('customerUnblacklistedEvent', { stateId: stateId, customerId: customer.id });
        } catch(error) {
            await this.brokerPublisher.dispatch('unblacklistCustomerFailedEvent', { stateId: stateId, error: error.message });
        }
    }

}