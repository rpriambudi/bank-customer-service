import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { CustomerService } from './../../interfaces/services/customer-service.interface';

export class RollbackUnblacklistCustomerHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly customerService: CustomerService
    ) {
        super();
    }

    getCommandName(): string {
        return 'rollbackUnblacklistCustomerCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { stateId, cifNo } = data;

        try {
            const customer = await this.customerService.blacklistCustomer(cifNo);
            await this.brokerPublisher.dispatch('customerBlacklistedEvent', { stateId: stateId, customerId: customer.id });
        } catch(error) {
            await this.brokerPublisher.dispatch('blacklistCustomerFailedEvent', { stateId: stateId, error: error.message });
        }
    }

}