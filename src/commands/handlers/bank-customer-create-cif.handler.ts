import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { CifNoGenerator } from './../../interfaces/cif-no-generator.interface';

export class BankCustomerCreateCifHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly cifNoGenerator: CifNoGenerator
    ) {
        super();
    }

    getCommandName(): string {
        return 'bankCustomerCreateCifCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { stateId, branchCode, idNumber } = data;

        try {
            const cifNo = await this.cifNoGenerator.generateNewNumber(branchCode, idNumber);
            await this.brokerPublisher.dispatch('bankCustomerCifCreatedEvent', { stateId: stateId, cifNo: cifNo });
        } catch(error) {
            await this.brokerPublisher.dispatch('bankCustomerCreateCifFailedEvent', { stateId: stateId, error: error.message });
        }
    }

}