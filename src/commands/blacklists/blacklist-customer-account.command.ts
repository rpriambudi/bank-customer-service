import { BaseCommand } from 'bank-shared-lib';

export class BlacklistCustomerAccountCommand extends BaseCommand {
    getCommandName(): string {
        return 'blacklistCustomerAccountCommand';
    }
    
}