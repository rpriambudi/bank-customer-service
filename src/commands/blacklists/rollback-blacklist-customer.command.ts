import { BaseCommand } from 'bank-shared-lib';

export class RollbackBlacklistCustomerCommand extends BaseCommand {
    getCommandName(): string {
        return 'rollbackBlacklistCustomerCommand';
    }
    
}