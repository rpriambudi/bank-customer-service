export class CreateCustomerDto {
    name: string;

    address: string;

    birthDate: string;

    idType: string;

    idNumber: string;

    gender: string;

    motherMaidenName: string;

    registeredBy: string

    branchCode: string

    static buildFromJsonObject(jsonObj: any): CreateCustomerDto {
        return {
            name: jsonObj.name,
            address: jsonObj.address,
            birthDate: jsonObj.birthDate,
            idType: jsonObj.idType,
            idNumber: jsonObj.idNumber,
            gender: jsonObj.gender,
            motherMaidenName: jsonObj.motherMaidenName,
            registeredBy: jsonObj.motherMaidenName,
            branchCode: jsonObj.branchCode
        }
    }
}