import { Customer } from './../../entities/customer.entity';

export interface CustomerQueryService {
    findCustomerByCifNo(cifNo: string): Promise<Customer>;
    findCustomerByIdTypeAndNumber(idType: string, idNumber: string): Promise<Customer>;
    findValidCustomerForBlacklist(cifNo: string): Promise<Customer>;
    findValidCustomerForUnblacklist(cifNo: string): Promise<Customer>;
    findValidCustomerForApproval(cifNo: string): Promise<Customer>;
}