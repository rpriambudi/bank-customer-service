export interface CifNoGenerator {
    generateNewNumber(branchCode: string, idNumber: string): Promise<string>;
}