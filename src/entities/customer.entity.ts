import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

export enum StatusEnum {
    Live = 'LIVE',
    Blacklisted = 'BLACKLISTED',
    OnApproval = 'ON_APPROVAL',
    Pending = 'PENDING'
}

@Entity()
export class Customer {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        unique: false,
        name: 'branch_id'
    })
    branchId: number;

    @Column({
        type: 'varchar',
        unique: true,
        name: 'cif_no',
        nullable: true
    })
    cifNo: string;

    @Column({
        type: 'varchar',
        unique: false,
        name: 'name'
    })
    name: string;

    @Column({
        type: 'text',
        unique: false,
        name: 'address'
    })
    address: string;
    
    @Column({
        type: 'date',
        unique: false,
        name: 'birth_date'
    })
    birthDate: string;

    @Column({
        type: 'varchar',
        unique: false,
        name: 'id_type'
    })
    idType: string;

    @Column({
        type: 'varchar',
        unique: true,
        name: 'id_number'
    })
    idNumber: string;

    @Column({
        type: 'varchar',
        unique: false,
        name: 'gender'
    })
    gender: string;

    @Column({
        type: 'varchar',
        unique: false,
        name: 'mother_maiden_name'
    })
    motherMaidenName: string;

    @Column({
        type: 'varchar',
        unique: false,
        name: 'status',
        default: StatusEnum.OnApproval
    })
    status: string;

    @Column({
        type: 'varchar',
        unique: false,
        name: 'registered_by'
    })
    registeredBy: string;

    @Column({
        type: 'timestamp',
        unique: false,
        name: 'registered_at',
        default: new Date()
    })
    registeredAt: string;
}