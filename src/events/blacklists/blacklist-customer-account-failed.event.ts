import { BaseEvent } from 'bank-shared-lib';

export class BlacklistCustomerAccountFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'blacklistCustomerAccountFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.error = eventData.error;
        return stateData;
    }
    
}