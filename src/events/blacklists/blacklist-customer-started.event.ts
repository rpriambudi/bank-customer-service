import { BaseEvent } from 'bank-shared-lib';

export class BlacklistCustomerStartedEvent extends BaseEvent {
    getEventName(): string {
        return 'blacklistCustomerStartedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}