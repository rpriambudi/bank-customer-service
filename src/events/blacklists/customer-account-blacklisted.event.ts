import { BaseEvent } from 'bank-shared-lib';

export class CustomerAccountBlacklistedEvent extends BaseEvent {
    getEventName(): string {
        return 'customerAccountBlacklistedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}