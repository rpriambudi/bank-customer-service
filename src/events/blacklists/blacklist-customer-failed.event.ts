import { BaseEvent } from 'bank-shared-lib';

export class BlacklistCustomerFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'blacklistCustomerFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}