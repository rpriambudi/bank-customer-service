import { BaseEvent } from 'bank-shared-lib';

export class CustomerBlacklistedEvent extends BaseEvent {
    getEventName(): string {
        return 'customerBlacklistedEvent';
    }
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}