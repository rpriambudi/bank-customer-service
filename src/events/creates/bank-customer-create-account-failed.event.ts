import { BaseEvent } from 'bank-shared-lib';

export class BankCustomerCreateAccountFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'bankCustomerCreateAccountFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.errorMessage = eventData.error;
        return stateData;
    }
    
}