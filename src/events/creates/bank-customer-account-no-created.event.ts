import { BaseEvent } from 'bank-shared-lib';

export class BankCustomerAccountNoCreatedEvent extends BaseEvent {
    getEventName(): string {
        return 'bankCustomerAccountNoCreatedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.accountNo = eventData.accountNo;
        return stateData;
    }
    
}