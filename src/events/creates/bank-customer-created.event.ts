import { BaseEvent } from 'bank-shared-lib';

export class BankCustomerCreatedEvent extends BaseEvent {
    getEventName(): string {
        return 'bankCustomerCreatedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.customerId = eventData.customerId;
        return stateData;
    }
    
}