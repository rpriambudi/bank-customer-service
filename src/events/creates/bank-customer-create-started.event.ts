import { BaseEvent } from 'bank-shared-lib';

export class BankCustomerCreateStartedEvent extends BaseEvent {
    getEventName(): string {
        return 'bankCustomerCreateStartedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}