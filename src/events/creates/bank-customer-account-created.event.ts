import { BaseEvent } from 'bank-shared-lib';

export class BankCustomerAccountCreatedEvent extends BaseEvent {
    getEventName(): string {
        return 'bankCustomerAccountCreatedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.accountId = eventData.accountId;
        return stateData;
    }
    
}