import { BaseEvent } from 'bank-shared-lib';

export class BankCustomerCifCreatedEvent extends BaseEvent {
    getEventName(): string {
        return 'bankCustomerCifCreatedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.cifNo = eventData.cifNo;
        return stateData;
    }
    
}