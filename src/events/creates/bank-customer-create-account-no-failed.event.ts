import { BaseEvent } from 'bank-shared-lib';

export class BankCustomerCreateAccountNoFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'bankCustomerCreateAccountNoFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.errorMessage = eventData.error;
        return stateData;
    }
    
}