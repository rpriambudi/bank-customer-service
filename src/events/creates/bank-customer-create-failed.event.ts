import { BaseEvent } from 'bank-shared-lib';

export class BankCustomerCreateFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'bankCustomerCreateFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.errorMessage = eventData.error;
        return stateData;
    }
    
}