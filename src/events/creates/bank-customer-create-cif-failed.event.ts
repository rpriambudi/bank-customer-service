import { BaseEvent } from 'bank-shared-lib';

export class BankCustomerCreateCifFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'bankCustomerCreateCifFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.errorMessage = eventData.error;
        return stateData;
    }
    
}