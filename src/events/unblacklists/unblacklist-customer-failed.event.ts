import { BaseEvent } from 'bank-shared-lib';

export class UnblacklistCustomerFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'unblacklistCustomerFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}