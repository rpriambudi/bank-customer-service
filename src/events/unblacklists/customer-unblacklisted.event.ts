import { BaseEvent } from 'bank-shared-lib';

export class CustomerUnblacklistedEvent extends BaseEvent {
    getEventName(): string {
        return 'customerUnblacklistedEvent';
    }
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}