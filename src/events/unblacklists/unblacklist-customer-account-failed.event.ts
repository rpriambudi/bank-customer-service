import { BaseEvent } from 'bank-shared-lib';

export class UnblacklistCustomerAccountFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'unblacklistCustomerAccountFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.error = eventData.error;
        return stateData;
    }
    
}