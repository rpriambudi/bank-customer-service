import { BaseEvent } from 'bank-shared-lib';

export class UnblacklistCustomerStartedEvent extends BaseEvent {
    getEventName(): string {
        return 'unblacklistCustomerStartedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}