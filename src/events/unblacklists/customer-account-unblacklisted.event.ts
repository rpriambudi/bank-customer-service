import { BaseEvent } from 'bank-shared-lib';

export class CustomerAccountUnblacklistedEvent extends BaseEvent {
    getEventName(): string {
        return 'customerAccountUnblacklistedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}