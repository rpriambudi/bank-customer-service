import {MigrationInterface, QueryRunner} from "typeorm";

export class SequenceMigration1587879332194 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('CREATE SEQUENCE IF NOT EXISTS  cif_no_sequence');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DROP SEQUENCE IF EXISTS cif_no_sequence');
    }

}
