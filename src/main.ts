import { NestFactory } from '@nestjs/core';
import { IBrokerConsumer, HttpGenericFilter } from 'bank-shared-lib';
import { AppModule } from './app.module';
import exceptionCode from './../exception-code.json';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const saga = app.select(AppModule).get<IBrokerConsumer>('SagaExecutor');
  saga.consume();
  const consumer = app.select(AppModule).get<IBrokerConsumer>('BrokerConsumer');
  consumer.consume();
  app.useGlobalFilters(new HttpGenericFilter(exceptionCode));
  await app.listen(3003);
}
bootstrap();
