import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from './../entities/customer.entity';
import { CifNoGenerator } from './../interfaces/cif-no-generator.interface';
import { CustomerRepository } from './../repositories/customer.repository';

@Injectable()
export class SequenceCifNoGenerator implements CifNoGenerator {
    constructor(
        @InjectRepository(Customer) private readonly customerRepository: CustomerRepository,
        private readonly configService: ConfigService
    ) {}
    async generateNewNumber(branchCode: string, idNumber: string): Promise<string> {
        const idSeq = idNumber.slice(0, 3);
        let latestSeq = await this.customerRepository.getSequence(this.configService.get<string>('customer.sequenceName'));
        
        if (latestSeq.length < 16) {
            const trailingZeroSize = 16 - latestSeq.length;
            let trailingZero = ''
            for (let i = 0; i < trailingZeroSize; i++) {
                trailingZero += '0'
            }
            latestSeq = trailingZero + latestSeq
        }

        return branchCode + '' + idSeq + '' + latestSeq;
    }
    
}