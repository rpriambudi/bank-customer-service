import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from './../../entities/customer.entity';
import { CreateCustomerDto } from './../../dto/create-customer.dto';
import { BusinessRule } from './../../interfaces/business-rule.interface';
import { CustomerRepository } from './../../repositories/customer.repository';

export class NewCustomerValidation implements BusinessRule {
    constructor(
        @InjectRepository(Customer) private readonly customerRepository: CustomerRepository
    ) {}

    async validate(createCustomerDto: CreateCustomerDto): Promise<void> {
        const customer = await this.customerRepository.findOne({ idType: createCustomerDto.idType, idNumber: createCustomerDto.idNumber });
        if (customer)
            throw new Error('Customer is exists');
    }
}