import { BusinessRule } from './../../interfaces/business-rule.interface';
import { Customer, StatusEnum  } from './../../entities/customer.entity';
import { InvalidStatusException } from './../../exceptions/invalid-status.exception';

export class BlacklistStatusRule implements BusinessRule {
    async validate(customerData: Customer): Promise<void> {
        if (customerData.status === StatusEnum.Blacklisted)
            throw new InvalidStatusException('Operation not permitted. Customer already blacklisted.')
    }
}

export class ApproveStatusRule implements BusinessRule {
    async validate(customerData: Customer): Promise<void> {
        if (customerData.status === StatusEnum.Live)
            throw new InvalidStatusException('Operation not permitted. Customer already approved.')

        if (customerData.status === StatusEnum.Blacklisted)
            throw new InvalidStatusException('Operation not permitted. Customer is blacklisted.')
    }
}

export class UnblacklistStatusRule implements BusinessRule {
    async validate(customerData: Customer): Promise<void> {
        if (customerData.status === StatusEnum.Live || customerData.status === StatusEnum.OnApproval)
            throw new InvalidStatusException('Operation not permitted. Customer is not blacklisted')
    }
}

export class ApprovalStatusRule implements BusinessRule {
    async validate(customerData: Customer): Promise<void> {
        if (customerData.status === StatusEnum.OnApproval)
            throw new InvalidStatusException('Operation not permitted. Customer has not been approved');
    }
}