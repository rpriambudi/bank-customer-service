import { Repository } from 'typeorm';
import { Customer, StatusEnum } from './../../entities/customer.entity';
import { CreateCustomerDto } from './../../dto/create-customer.dto';
import { CustomerService } from './../../interfaces/services/customer-service.interface';
import { CustomerFactory } from './../../factories/customer.factory';
import { CustomerQueryService } from './../../interfaces/services/customer-query-service.interface';

export class CustomerServiceImpl implements CustomerService {
    constructor(
        private readonly repository: Repository<Customer>,
        private readonly customerFactory: CustomerFactory,
        private readonly customerQueryService: CustomerQueryService
    ) {}

    async createCustomer(createCustomerDto: CreateCustomerDto, cifNo: string): Promise<Customer> {
        const customerData = await this.customerFactory.buildNewCustomerData(createCustomerDto, cifNo);
        return await this.repository.save(customerData);
    }

    async createCustomerWithoutCif(createCustomerDto): Promise<Customer> {
        const customerData = await this.customerFactory.buildNewCustomerData(createCustomerDto);
        return await this.repository.save(customerData);
    }
    
    async blacklistCustomer(cifNo: string): Promise<Customer> {
        const customer = await this.customerQueryService.findValidCustomerForBlacklist(cifNo);
        return await this.updateCustomerStatus(customer, StatusEnum.Blacklisted);
    }

    async unblacklistCustomer(cifNo: string): Promise<Customer> {
        const customer = await this.customerQueryService.findValidCustomerForUnblacklist(cifNo);
        return await this.updateCustomerStatus(customer, StatusEnum.OnApproval);
    }

    async approveCustomer(cifNo: string): Promise<Customer> {
        const customer = await this.customerQueryService.findValidCustomerForApproval(cifNo);
        return await this.updateCustomerStatus(customer, StatusEnum.Live);
    }

    private async updateCustomerStatus(customer: Customer, status: StatusEnum) {
        customer.status = status;
        return await this.repository.save(customer);
    }
}