import { Repository } from 'typeorm';
import { RuleContext } from './../rule-context.impl';
import { Customer } from './../../entities/customer.entity';
import { CustomerQueryService } from './../../interfaces/services/customer-query-service.interface';
import { CustomerNotFoundException } from './../../exceptions/customer-not-found.exception';

export class CustomerQueryServiceImpl implements CustomerQueryService {
    constructor(
        private readonly repository: Repository<Customer>,
        private readonly blacklistRule: RuleContext,
        private readonly unblacklistRule: RuleContext,
        private readonly approvalRule: RuleContext
    ) {}

    async findCustomerByCifNo(cifNo: string): Promise<Customer> {
        const customerData = await this.repository.findOne({cifNo: cifNo});
        if (!customerData)
            throw new CustomerNotFoundException('Customer not found.');
            
        return customerData;
    }    
    
    async findCustomerByIdTypeAndNumber(idType: string, idNumber: string): Promise<Customer> {
        return await this.repository.findOne({ idType: idType, idNumber: idNumber });
    }

    async findValidCustomerForBlacklist(cifNo: string): Promise<Customer> {
        const customer = await this.findCustomerByCifNo(cifNo);
        await this.blacklistRule.validate(customer);
        return customer;
    }

    async findValidCustomerForUnblacklist(cifNo: string): Promise<Customer> {
        const customer = await this.findCustomerByCifNo(cifNo);
        await this.unblacklistRule.validate(customer);
        return customer;
    }

    async findValidCustomerForApproval(cifNo: string): Promise<Customer> {
        const customer = await this.findCustomerByCifNo(cifNo);
        await this.approvalRule.validate(customer);
        return customer;
    }
}